<?php

/**
 * @file
 * File Force imagecache Download module.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * Add file_force formatters to file/image fields.
 */
function file_force_imagecache_field_formatter_info() {
  $formatters = array();

  // Handle image field files.
  $formatters['file_force_image_imagecache'] = array(
    'label' => t('File Force: Image (imagecache preprocessed)'),
    'field types' => array('image'),
    'settings' => array('image_style' => '', 'image_style_download' => ''),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_view().
 */
function file_force_imagecache_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $separator = variable_get('clean_url', FALSE) ? '?' : '&';

  switch ($display['type']) {
    case 'file_force_image_imagecache':
      $element = array();

      module_load_include('module', 'file_force');
      foreach ($items as $delta => $item) {
        $uri = array(
          'path' => file_force_create_url(image_style_path($display['settings']['image_style_download'], $item['uri']), FALSE),
          'options' => array('query' => array('download' => '1')),
        );
        $element[$delta] = array(
          '#theme' => 'image_formatter',
          '#item' => $item,
          '#image_style' => $display['settings']['image_style'],
          '#path' => $uri,
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function file_force_imagecache_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  if ($display['type'] == 'file_force_image_imagecache') {
    $settings = $display['settings'];

    $image_styles = image_style_options(FALSE);
    $element['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $settings['image_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );

    $element['image_style_download'] = array(
      '#title' => t('Image style for download'),
      '#type' => 'select',
      '#default_value' => $settings['image_style_download'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );

    return $element;
  }
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function file_force_imagecache_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  if ($display['type'] == 'file_force_image_imagecache') {
    $settings = $display['settings'];

    $summary = array();

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    if (isset($image_styles[$settings['image_style']])) {
      $summary[] = t('Preview image style: @preview_image_style, Download image style: @image_style_download', array('@preview_image_style' => $image_styles[$settings['image_style']], '@image_style_download' => $image_styles[$settings['image_style_download']]));
    }
    else {
      $summary[] = t('Original image');
    }

    $summary[] = t('Linked to file');

    return implode('<br />', $summary);
  }
}
